# Backup Ceph S3

A Python container with the [boto3](https://github.com/boto/boto3) library and some scripts to interact between a Kubernetes cluster and a S3 system using the same api as aws S3.

## How to use this image

This image is destined to be used in kubernetes, there is some yaml examples in the deploy/k8s directory. (but you could use it locally if needed by adding the right environment variables and volumes to your container)
Basically, in the yaml file you need environment variables from a secret with your S3 credentials (you have to create this secret), the volume where there is your data to backup and you need to change the `command` and `args` of the container to choose your action.

3 scripts are shipped in the image.

The first one to test the the connection to the S3 system, it will list all the buckets you have access to :

```python
python test_connection.py
```

The second one is to upload / backup a file or directory to S3 :

```bash
$ python backup.py -h
usage: backup.py [-h] [-c] [-q] [--verbose] bucket dataPath fileName

A script to do backups on S3 systems (using same API as Amazon S3) with boto3, you can backup a file or a directory if you compress it (you can also compress a single file). This script is shipped in a container image to
be used on Kubernetes. It needs 3 environment variables to run : URL, ACCESSKEY and SECRETKEY (the info to access S3)

positional arguments:
  bucket          Name of the bucket where you want to upload your backup
  dataPath        Where the file or directory to backup is located (put absolute path with the file/directory name)
  fileName        The name of your backup file once on S3

optional arguments:
  -h, --help      show this help message and exit
  -c, --compress  Backup the file making it a compressed archive (dont forget the .tgz at the end of the file name)
  -q, --quiet     Disable logging
  --verbose, -v   There is 2 levels of verbosity, juste use the flag once or twice
```

You can use this script with a job or a cronjob.

The last script will download a file from S3 to a volume :

```bash
$ python restore.py -h
usage: restore.py [-h] [--date DATE] [-c] [-q] [-v] bucket fileName dataPath

A script to download a file from S3, if the file is a tar archive, the script can extract it. This script is shipped in a container image to be used on Kubernetes. It needs 3 environment variables to run : URL, ACCESSKEY
and SECRETKEY (the info to access S3)

positional arguments:
  bucket            Name of the bucket where you want to fetch your file
  fileName          The name of your file in the bucket
  dataPath          The path to the directory where you want to download your file.

optional arguments:
  -h, --help        show this help message and exit
  --date DATE       Use this flag if your file is in a versioned bucket, and precise the date after the flag using this format : 'YYYY-MM-DD HH:MM:SS' UTC time. If you dont use this flag, the latest version will be
                    downloaded.
  -c, --compressed  use this flag if your file is a tar archive and you want to extract it once downloaded.
  -q, --quiet       Disable logging
  -v, --verbose     There is 2 levels of verbosity, juste use the flag once or twice
```

**WARNING** : If you want to backup a directory, you have to use th `-c` flag to compress your directory, the script will fail otherwise.

## Some infos about S3

### How to access S3

There is different solutions to access to and consult your S3 system. You can have a web ui or you can use a CLI.  
A really handy one is the [minio cli](https://github.com/minio/mc), but it does not support every feature of S3 (like versionning), you can use `mc config host add` to add as many accounts as you would like.

The other one you can use is the [aws cli](https://aws.amazon.com/fr/cli/), simply do `aws configure` to use your credentials. The default is that you can only use one account at a time and have to do `aws configure` each time you need to change of access key and secret key.  
If your S3 system is not at aws, you can use this command `aws --endpoint=https://otherS3endpoint.com s3api` followed by what you need.  
Some useful commands :  

To list all your buckets

```bash
aws --endpoint=https://cccephs3.in2p3.fr:8080 s3api list-buckets
```

To list all the objects versions of a bucket with their date (useful for the restore script)

```bash
aws --endpoint=https://cccephs3.in2p3.fr:8080 s3api list-object-versions --bucket my-bucket
```

### How to activate bucket versioning

To do that simply use the command :

```bash
aws --endpoint=https://cccephs3.in2p3.fr:8080 s3api put-bucket-versioning --bucket my-bucket --versioning-configuration Status=Enabled
```

For more information about bucket versioning you can read [this](https://docs.aws.amazon.com/AmazonS3/latest/dev/Versioning.html) or [this](https://ceph.io/planet/on-ceph-rgw-s3-object-versioning/)

### How to manage bucket lifecycle

Lifecycle management of buckets is useful if you don't want to manage yourself your different backups. It can delete or move files to an other type of storage following a set of rules you defined. You have information about this [here](https://docs.aws.amazon.com/AmazonS3/latest/dev/object-lifecycle-mgmt.html).

A simple example :

```bash
aws --endpoint=https://cccephs3.in2p3.fr:8080 s3api put-bucket-lifecycle --bucket my-bucket --lifecycle-configuration file://lifecycle.json
```

lifecycle.json :

```json
{
    "Rules": [
        {
            "ID": "1-year-expiration",
            "Prefix": "",
            "Status": "Enabled",
            "NoncurrentVersionExpiration": {
                "NoncurrentDays": 365
            }
        }
    ]
}
```
