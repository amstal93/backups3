import boto3
import datetime
import pytz
import tarfile
import os
import logging
import argparse

parser = argparse.ArgumentParser(description='''A script to download a file from S3, if the file is a tar archive, the script can extract it.
                                                This script is shipped in a container image to be used on Kubernetes. It needs 3 environment
                                                variables to run : URL, ACCESSKEY and SECRETKEY (the info to access S3)''')
parser.add_argument("bucket", type=str, help="Name of the bucket where you want to fetch your file")
parser.add_argument("fileName", type=str, help="The name of your file in the bucket")
parser.add_argument("dataPath", type=str, help="The path to the directory where you want to download your file.")

parser.add_argument("--date", help="Use this flag if your file is in a versioned bucket, and precise the date after the flag using this format : 'YYYY-MM-DD HH:MM:SS' UTC time. If you don't use this flag, the latest version will be downloaded.")
parser.add_argument("-c", "--compressed", action="store_true", help="use this flag if your file is a tar archive and you want to extract it once downloaded.")
parser.add_argument("-q", "--quiet", action="store_true", help="Disable logging")
parser.add_argument("-v", "--verbose", action="count", default=0, help="There is 2 levels of verbosity, juste use the flag once or twice")
args = parser.parse_args()

if args.quiet:
    logging.disabled = True
elif args.verbose>=2:
    logging.basicConfig(format='%(asctime)s:%(levelname)s: %(message)s', level=logging.DEBUG)
elif args.verbose==1:
    logging.basicConfig(format='%(asctime)s:%(levelname)s: %(message)s', level=logging.INFO)
else:
    logging.basicConfig(format='%(asctime)s:%(levelname)s: %(message)s', level=logging.WARNING)

bucket=args.bucket
logging.debug("The value of Bucket is : " + bucket)
fileName=args.fileName
logging.debug("The value of fileName is : " + fileName)
dataPath=args.dataPath
logging.debug("The value of dataPath is : " + dataPath)

s3 = boto3.resource('s3',
                    endpoint_url=os.environ.get('URL'),
                    aws_access_key_id=os.environ.get('ACCESSKEY'),
                    aws_secret_access_key=os.environ.get('SECRETKEY'))

logging.info("Accessing S3 at endpoint : " + os.environ.get('URL') + " , with accessKey : " + os.environ.get('ACCESSKEY'))

if (args.date):
    # format : 'YYYY-MM-DD HH:MM:SS' heure UTC
    version_date = pytz.timezone('UTC').localize(datetime.datetime.strptime(args.date, '%Y-%m-%d %H:%M:%S'))
    versions = s3.Bucket(bucket).object_versions.filter(Prefix=fileName)
    for version in versions:
        obj = version.get()
        logging.debug(obj.get('VersionId'), obj.get('ContentLength'), obj.get('LastModified'))
        if (obj.get('LastModified')==version_date):
            obj_version=obj.get('VersionId')
            logging.info("Found verionId : " + obj_version + " at date : " + version_date.strftime('%Y-%m-%d %H:%M:%S'))
    logging.info("Downloading requested version ...")
    s3.meta.client.download_file(bucket, fileName, dataPath + fileName , ExtraArgs={'VersionId': obj_version})
else:
    logging.info("Downloading latest version ...")
    s3.meta.client.download_file(bucket, fileName, dataPath + fileName)
    
logging.info("Download complete !")

if (args.compressed):
    logging.info("Extracting archive : " + fileName + " to " + dataPath)
    my_tar = tarfile.open(dataPath + fileName)
    my_tar.extractall(dataPath)
    my_tar.close()
    os.remove(dataPath + fileName)
    logging.info("Done !")
