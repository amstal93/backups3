FROM python:3.6.10-alpine3.11
RUN pip install boto3==1.13.21 pytz==2020.1
WORKDIR /src/scripts
COPY *.py ./
ENTRYPOINT ["python"]
CMD ["test_connection_s3.py"]
